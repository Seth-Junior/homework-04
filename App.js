import React from 'react';
import { NativeBaseProvider} from 'native-base';
import ABARoot from './src/views/Root'

export default function App() {
  return (
      <ABARoot/>
  );
}