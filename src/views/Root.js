import React from 'react';
import { StyleSheet } from 'react-native';
import { NativeBaseProvider, View } from 'native-base';
import ABAHeader from './ABAHeader'
import ABAContent from './ABAContent'
import ABAFooter from './ABAFooter'

export default function Root() {
    return (
        <NativeBaseProvider>
            <ABAHeader />
            <ABAContent />
            <ABAFooter />
        </NativeBaseProvider>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'red',
        // padding: 20
    }
})