import React from 'react';
import { View, Text, HStack, IconButton, Icon } from 'native-base';
import Fontisto from 'react-native-vector-icons/Fontisto';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { StyleSheet } from 'react-native';

const ABAFooter = () => {
  return (
    <View style={{ height: '40%' }}>

      <HStack style={styles.hstackStyle}>
        <View style={styles.textWrap}>
          <Text style={{ fontSize: 18, color: 'white', marginBottom: 5 }}>Quick Transfer</Text>
          <Text style={{ fontSize: 13, color: 'white' }}>Create your templates here to make transfer quicker</Text>
        </View>
        <IconButton
          icon={
            <Icon
              size='70'
              as={<Fontisto name="arrow-swap" />}
              color="gray"
              opacity={0.3}
              style={{marginTop:'50%',right:'-20%'}}
            />
          }
        />
      </HStack>

      <HStack style={[styles.hstackStyle, { backgroundColor: '#fa8072' }]}>
        <View style={styles.textWrap}>
          <Text style={{ fontSize: 18, color: 'white', marginBottom: 5 }}>Quick Payment</Text>
          <Text style={{ fontSize: 13, color: 'white' }}>Paying your bills with templates is faster</Text>
        </View>
        <IconButton
          icon={
            <Icon
              size='120'
              as={<MaterialCommunityIcons name="currency-usd-circle-outline" />}
              color="gray"
              opacity={0.3}
              style={{marginTop:'50%',right:'-40%'}}
            />
          }
        />
      </HStack>

    </View>
  );
};

const styles = StyleSheet.create({
  hstackStyle: {
    height: '50%',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#00BFFF'
  },
  textWrap: {
    width: '60%',
    margin: 20,
    flexDirection: 'column',
    justifyContent: 'center'
  }
})

export default ABAFooter;
