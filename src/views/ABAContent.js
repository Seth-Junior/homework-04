import {
  HStack,
  Divider,
  IconButton,
  Icon,
  Text,
} from 'native-base';
import { View, StyleSheet } from 'react-native';
import React from 'react';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const ABAContent = () => {
  return (
    <View style={{ height: '50%', backgroundColor: '#002c40' }}>
      <HStack style={{ height: '33.2%'}}>
        <View style={styles.content}>
          <IconButton
            icon={
              <Icon
                size="md"
                as={<FontAwesome name="wallet" />}
                color="white"
              />
            }
          />
          <Text style={styles.text}>
            Accounts
          </Text>
        </View>
        <Divider style={{ borderColor: 'white', height: '100%' }} />
        <View style={styles.content}>
          <IconButton
            icon={
              <Icon size="md" as={<Ionicons name="card" />} color="white" />
            }
          />
          <Text style={styles.text}>
            Cards
          </Text>
        </View>
        <Divider style={{ borderColor: 'white', height: '100%' }} />
        <View style={styles.content}>
          <IconButton
            icon={
              <Icon
                size="md"
                as={<MaterialCommunityIcons name="currency-usd-circle-outline" />}
                color="white"
              />
            }
          />
          <Text style={styles.text}>
            Payments
          </Text>
        </View>
      </HStack>
      <Divider style={styles.vdivider} />
        <HStack style={{ height: '33.2%'}}>
          <View style={styles.content}>
            <IconButton
              icon={
                <Icon
                  size="md"
                  as={<Ionicons name="newspaper" />}
                  color="white"
                />
              }
            />
            <Text style={styles.text}>
              New Account
            </Text>
          </View>
          <Divider style={styles.divederStyle} />
          <View style={styles.content}>
            <IconButton
              icon={
                <Icon
                  size="md"
                  as={<FontAwesome name="internet-explorer" />}
                  color="white"
                />
              }
            />
            <Text style={styles.text}>
              Cash to ATM
            </Text>
          </View>
          <Divider style={styles.divederStyle} />
          <View style={styles.content}>
            <IconButton
              icon={
                <Icon
                  size="md"
                  as={<Fontisto name="arrow-swap" />}
                  color="white"
                />
              }
            />
            <Text style={styles.text}>
              Transfer
            </Text>
          </View>
        </HStack>
        <Divider style={styles.vdivider} />
        <HStack style={{ height: '33.2%'}}>
          <View style={styles.content}>
            <IconButton
              icon={
                <Icon
                  size="md"
                  as={<Ionicons name="qr-code" />}
                  color="white"
                />
              }
            />
            <Text style={styles.text}>
              Scan QR
            </Text>
          </View>
          <Divider style={styles.divederStyle} />
          <View style={styles.content}>
            <IconButton
              icon={
                <Icon
                  size="md"
                  as={<FontAwesome name="hand-holding-usd" />}
                  color="white"
                />
              }
            />
            <Text style={styles.text}>
              Loads
            </Text>
          </View>
          <Divider style={styles.divederStyle} />
          <View style={styles.content}>
            <IconButton
              icon={
                <Icon
                  size="md"
                  as={<FontAwesome name="map-marked-alt" />}
                  color="white"
                />
              }
            />
            <Text style={styles.text}>
              Locator
            </Text>
          </View> 
        </HStack>
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    width: '33.3%',
    height: '100%',
    justifyContent:'center',
    alignItems:'center',
    flexDirection: 'column',
  },
  text:{
    color:"white" ,
    fontSize:14 ,
  },
  divederStyle:{
    borderColor: 'white', 
    height: '100%'
  },
  vdivider:{
    width: '90%',
    borderColor: 'white',
    marginLeft:'5%'
  },
});

export default ABAContent;
