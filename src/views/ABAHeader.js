import React from 'react';
import { StatusBar, Box, HStack, IconButton, Text, Icon , View } from 'native-base';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';

export default function ABAHeader () {
  return (
    <View style={{height:'10%'}}>
      <StatusBar backgroundColor="#002c40" barStyle="light-content" />
      <HStack
          bg="#00415f"
          px="3"
          py="2"
          justifyContent="space-between"
          alignItems="center"
          height="100%"
        >
        <HStack space="3" alignItems="center">
          <IconButton
            icon={
              <Icon
                size="sm"
                as={<AntDesign name="bars" />}
                color="white"
              />
            }
          />
          <Text color="white" fontSize="22" fontWeight="bold">
            ABA Mobile
          </Text>
        </HStack>
        <HStack space="3">
          <IconButton
            icon={
              <Icon
                as={<AntDesign name="bells" />}
                size="sm"
                color="white"
              />
            }
          />
          <IconButton
            icon={
              <Icon
                as={<Feather name="phone-call" />}
                color="white"
                size='sm'
              />
            }
          />
        </HStack>
      </HStack>
    </View>
  );
};

